//gestion du boutton burger
const bgr = document.getElementById("bgr")
 
 bgr.addEventListener('click', () => {
        const target = bgr.dataset.target;
        const $target = document.getElementById(target);
        bgr.classList.toggle('is-active');
        $target.classList.toggle('is-active');
      });